<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Loading mySQL data via jQuery AJAX</title>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script >
$(document).ready(function() {
    $('button').click(function() {
        $.ajax({
            url: "ajax-jquery-php-data.php",
            success: function(r) {//success(result,status,xhr)
                $("div").html(r);
            },
            error: function(e) {//error(xhr,status,error)
                alert("ERROR: " + e.status + " " + e.statusText);
            }
        });
    });
});
</script>
</head>
<body>
<button>Load Data</button>
<div id="result"></div>
</body>
</html>
